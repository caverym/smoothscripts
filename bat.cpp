#include <unistd.h>
#include <iostream>
#include <cstdio>

#define mCat "cat, \"cat\""

const char *cat = "/usr/bin/cat";
const char *cap = "/sys/class/power_supply/BAT0/capacity";
const char *stat = "/sys/class/power_supply/BAT0/status";


void bCap() {
	execl(cat, "cat", cap, NULL);
}

void bStat() {
	execl(cat, "cat", stat, NULL);
}	

int main() {
	pid_t pid = fork();
	
	if (pid == 0) {
		bCap();
	} else if (pid > 0) {
		bStat();
	} else {
		printf("Failed to fork.");
		return 1;
	}

	return 0;
}
