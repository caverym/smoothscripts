#include <iostream>
#include <unistd.h>
#include <string>

// const char *vmauth = "vmware-authd.service";
const char *vmnet = "vmware-networks.service";
const char *vmusb = "vmware-usbarbitrator.service";

void vmmon(bool bleh) {
    if (bleh) {

        pid_t bl = fork();

        if (bl == 0) {
            std::cout << "Running Modprobe for vmw_vmci\n";
            execl("/usr/bin/modprobe", "modprobe", "vmw_vmci", NULL);
        }

        std::cout << "Running Modprobe for vmmon.\n";
        execl("/usr/bin/modprobe", "modprobe", "vmmon", "vmw_vmci", NULL);
    } else if (!bleh) {
        pid_t bl = fork();

        if (bl == 0) {
            std::cout << "Running rmmod for vmw_vmci.\n";
            execl("/usr/bin/rmmod", "rmmod", "vmw_vmci", NULL);
        }

        std::cout << "Running rmmod for vmmon.\n";
        execl("/usr/bin/rmmod", "rmmod", "vmmon", NULL);
    }
}

void commands_two(bool bleh) {
    if (bleh) {
        std::cout << "Enabling systectl services\n";
        execl("/usr/bin/systemctl", "systemctl", "enable", "--now", vmnet, vmusb, NULL);
    } else if (!bleh) {
        std::cout << "Disabling systemctl services\n";
        execl("/usr/bin/systemctl", "systemctl", "disable", "--now", vmnet, vmusb, NULL);
    }
}

void args(int argc, char *argv[]) {
    bool ar;
    if (argv[1] == nullptr) {
        std::cout << "Invalid arguments\n";
    }
    std::string b = argv[1];

    if (b == "enable") {
        ar = true;
    } else if (b == "disable") {
        ar = false;
    } else {
        std::cout << "Invalid arguments\n";
        exit(1);
    }

    pid_t pid = fork();
    if (pid == 0) {
        vmmon(ar);
    } else if (pid > 0) {
        commands_two(ar);
    } else {
        std::cout << "FAILURE\n";
        exit(1);
    }
}

int main(int argc, char *argv[]) {
    auto ID = geteuid();
    if (ID != 0) {
        std::cout << "You must be root.\n";
        exit(1);
    }
    args(argc, argv);
    return 0;
}