import os
import tempfile


def fscheck():
	tmp_handle, tmp_path = tempfile.mkstemp()
	if os.path.exists(tmp_path.upper()):
		print('Case insensitive.')
	else:
		print('Case sensitive.')


fscheck()
