#!/usr/bin/env python3
import os
import sys
from base64 import b64encode

length = int(sys.argv[1])
multi = length%3
if multi != 0:
    print('Must be a multiple of 3')
    sys.exit(1)
print(b64encode(os.urandom(length)).decode('utf-8'))
