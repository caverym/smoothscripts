#!/usr/bin/env python
import os
import time
import signal
import sys


clr = 'clear'


def cpu(os, time):
	os.system(clr)
	os.system('lscpu | grep MHz | sed 1!d')
	time.sleep(0.25)


def int_handle(signal, frame):
	os.system(clr)
	print('Killing...')
	sys.exit(0)


while 1 < 2:
	cpu(os, time)
	signal.signal(signal.SIGINT, int_handle)
